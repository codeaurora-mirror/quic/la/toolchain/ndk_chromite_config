#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Generates config_dump.json."""
import json
import os


def make_build(host):
    """Returns a configuration dict for the given host."""
    builder_class_name = 'config.builders.ndk_builders.Builder'

    if host == 'win32':
        host = 'windows'
    elif host == 'win64':
        host = 'windows64'

    return {
        'boards': [],
        'builder_class_name':  builder_class_name,
        'host': host,
    }


def main():
    manifest_repo_url = 'https://android.googlesource.com/platform/manifest'
    manifest_branch = 'master-ndk'

    config = {
        '_default': {
            'manifest_repo_url': manifest_repo_url,
            'manifest_branch': manifest_branch,
        },
        '_site_params': {},
        '_templates': {},
    }

    hosts = ['darwin', 'linux', 'win32', 'win64']

    for host in hosts:
        config[host] = make_build(host)

    this_dir = os.path.realpath(os.path.dirname(__file__))
    config_file_path = os.path.join(this_dir, 'config_dump.json')
    with open(config_file_path, 'w') as config_file:
        json.dump(config, config_file, sort_keys=True, indent=2,
                  separators=(',', ': '))


if __name__ == '__main__':
    main()
